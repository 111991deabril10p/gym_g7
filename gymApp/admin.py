from django.contrib import admin
from gymApp.models.exercises import Exercises
from gymApp.models.schedule import Schedule
from .models.coach import Coach
from .models.account import Account
from .models.group import Group

# Register your models here.

class CoachAdmi(admin.ModelAdmin):
    list_per_page = 20
    list_display = [ 'id' , 'name', 'email', 'phone']
    search_fields = ['comment']
    list_filter = ['speciality']

class ScheduleAdmi(admin.ModelAdmin):
    list_per_page = 20
    list_display = [ 'id' , 'fkid_Account', 'fkid_coach', 'date']
    search_fields = ['comment']
    list_filter = ['id']

class AccountAdmi(admin.ModelAdmin):
    list_per_page = 20
    list_display = [ 'id' , 'user', 'phoneNumber', 'city', 'weight', 'height', 'traineeDays', 'gender']
    search_fields = ['comment']
    list_filter = ['id']

class GroupAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('id','id', 'name', 'date')
    search_fields = ['name']
    list_filter = ('date',)

class ExerciseAdmi(admin.ModelAdmin):
    list_per_page = 20
    list_display = ['idExercise', 'name', 'description','image']
    search_fields = ['comment']
    list_filter = ['name']

admin.site.register(Coach, CoachAdmi)
admin.site.register(Schedule, ScheduleAdmi)
admin.site.register(Account, AccountAdmi)
admin.site.register(Group, GroupAdmin)
admin.site.register(Exercises, ExerciseAdmi)
