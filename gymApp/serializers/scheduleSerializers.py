from gymApp.models import Schedule
from rest_framework import serializers

class  ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = ['id', 'fkid_Account', 'fkid_coach', 'date']