from rest_framework import serializers
from gymApp.models.group import Group

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        #Que queremos mostrar
        fields = ['id','name','date', 'group_type','group_class']
