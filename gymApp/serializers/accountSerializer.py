from rest_framework import serializers
from gymApp.models.account import Account

class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['id', 'user', 'phoneNumber', 'city', 'weight', 'height', 'traineeDays', 'gender']