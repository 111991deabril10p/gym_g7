from rest_framework import serializers
from gymApp.models.exercises import Exercises

class ExerciseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Exercises
        fields = ['idExercise', 'name', 'description','image']