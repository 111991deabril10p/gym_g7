from gymApp.models import Coach
from rest_framework import serializers

class   CoachSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coach
        fields = ['id', 'name', 'speciality', 'email', 'phone']