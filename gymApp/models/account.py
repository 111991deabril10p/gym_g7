from django.db import models
from django.contrib.auth.models import User

class Account(models.Model):

    id = models.AutoField(primary_key=True)

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phoneNumber = models.CharField(max_length = 50, null=False)
    city = models.CharField(max_length = 50, null=False)
    weight = models.CharField(max_length = 30, null=False)
    height = models.CharField(max_length = 50, null=False)
    traineeDays = models.CharField(max_length = 50, null=False)
    gender = models.CharField(max_length = 64, choices=(
        ('F', 'Femenino'),
        ('M', 'Masculino'),
    ))


    
    

