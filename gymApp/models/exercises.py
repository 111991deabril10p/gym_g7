from django.db import models


class Exercises(models.Model):
    idExercise = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    description = models.TextField
    image = models.ImageField()
