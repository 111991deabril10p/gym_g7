from django.db import models

class Coach(models.Model):

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    speciality = models.CharField(max_length=50)
    email = models.CharField(max_length=64)
    phone = models.CharField(max_length=10)