from django.db import models

from gymApp.models.coach import Coach
from gymApp.models.account import Account

class Schedule(models.Model):
    id = models.AutoField(primary_key=True)
    fkid_Account = models.ForeignKey(Account, related_name="Mi_cuenta", on_delete=models.CASCADE, null=True)
    fkid_coach = models.ForeignKey(Coach, related_name='Mi_entrenador', on_delete=models.CASCADE, null=True)
    date =models.DateField(null=False)
