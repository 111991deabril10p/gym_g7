from django.db import models
from .account import Account
from .coach import Coach

class Group(models.Model):
    id = models.AutoField(primary_key=True)
    id = models.AutoField(primary_key=True)
    date = models.DateTimeField(null=True)
    name =models.CharField(max_length=20)
    group_type = models.CharField(max_length=64, choices=(
        ('B', 'basico'),
        ('M', 'Medio'),
        ('A', 'Avanzado'),
    )) 
    group_class = models.CharField(max_length=86, choices=(
        ('E', 'Aerobic'),
        ('R', 'Aerobox'),
        ('Q', 'Aquagym'),
        ('O', 'BodyJam'),
        ('C', 'Calistenia'),
        ('J', 'Jukari'),
    )) 
