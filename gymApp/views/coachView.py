from rest_framework import generics
from gymApp.models import Coach
from gymApp.serializers import CoachSerializer
from rest_framework import views, status
from rest_framework.response import Response


# List, Create
class CoachListCreateView(generics.ListCreateAPIView):
    queryset = Coach.objects.all()
    serializer_class = CoachSerializer

# Read, Update, Delete
class CoachRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Coach.objects.all()
    serializer_class = CoachSerializer 