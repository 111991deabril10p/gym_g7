from rest_framework import views, status
from rest_framework.response import Response
from rest_framework import generics
#modelo
from gymApp.models import Group
#serializador
from gymApp.serializers import GroupSerializer

# List, Create
class GroupListCreateView(generics.ListCreateAPIView):   
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

#(RetrieveUpdateDestroyAPIView)borrar,leer y actualizar las listas más rapido 
# Read, Update, Delete
class GroupRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
 