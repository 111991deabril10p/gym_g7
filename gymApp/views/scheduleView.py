from rest_framework import generics
from gymApp.models import Schedule
from gymApp.serializers.scheduleSerializers import ScheduleSerializer

# List, Create
class ScheduleListCreateView(generics.ListCreateAPIView):
    queryset = Schedule.objects.all()
    serializer_class = ScheduleSerializer
   

# Read, Update, Delete
class ScheduleRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Schedule.objects.all()
    serializer_class = ScheduleSerializer