from rest_framework import generics
from gymApp.models.exercises import Exercises
from gymApp.serializers.exerciseSerializer import ExerciseSerializer

# List, Create


class ExerciseListCreateView(generics.ListCreateAPIView):
    queryset = Exercises.objects.all()
    serializer_class = ExerciseSerializer


# Read, Update, Delete
class ExerciseRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Exercises.objects.all()
    serializer_class = ExerciseSerializer
