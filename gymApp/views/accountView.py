from rest_framework import generics
from gymApp.models import Account

from rest_framework import views, status
from rest_framework.response import Response

# Serializer
from gymApp.serializers import AccountSerializer


# List, Create
class AccountListCreateView(generics.ListCreateAPIView):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer

# Read, Update, Delete
class AccountRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer

