"""gymProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.sites.models import Site

from rest_framework import views

from gymApp import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', views.AccountListCreateView.as_view()),
    path('account/<int:pk>', views.AccountRetrieveUpdateDestroy.as_view()),

    path('coach/', views.CoachListCreateView.as_view()),
    path('coach/<int:pk>', views.CoachRetrieveUpdateDestroy.as_view()),

    path('schedule/', views.ScheduleListCreateView.as_view()),
    path('schedule/<int:pk>', views.ScheduleRetrieveUpdateDestroy.as_view()),

    path('execises/', views.ExerciseListCreateView.as_view()),
    path('execises/<int:pk>', views.ExerciseRetrieveUpdateDestroy.as_view()),

    path('groups/', views.GroupListCreateView.as_view()),
    path('group/<int:pk>', views.GroupRetrieveUpdateDestroy.as_view()),

    path('rest-auth/', include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls'))
]

admin.site.unregister(Site)
